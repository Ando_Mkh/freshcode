import { Module } from '@nestjs/common';
import { DatabaseModule } from '@boilerplate/data';
import { PassportModule } from '@nestjs/passport';
import { JwtModule } from '@nestjs/jwt';
import { UsersService } from './users.service';
import { TestService } from './test/test.service';
import { jwtConstants } from './constants';
import { AuthService } from './auth.service';
import { JwtStrategy } from './strategy/jwt.strategy';
import { JwtAuthGuard } from './guard/jwt.guard';
import { LocalStrategy } from './strategy/local.strategy';

@Module({
  imports: [
    DatabaseModule,
		PassportModule,
		JwtModule.register({
			secret: jwtConstants.secret,
			signOptions: {
				expiresIn: jwtConstants.expiresIn
			}
		}),
  ],
  providers: [
    UsersService,
    TestService,
		AuthService,
		JwtStrategy,
		JwtAuthGuard,
		LocalStrategy,
  ],
  exports: [
    UsersService,
		AuthService,
  ],
})
export class ServicesModule {}
