export const UsersRepositoryToken = 'USERS_REPOSITORY';

export const jwtConstants = {
	secret: 'secretKey',
	expiresIn: '24h'
};
