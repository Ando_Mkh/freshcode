import { Test, TestingModule } from '@nestjs/testing';
import { TestService } from './test.service';
import { UsersRepositoryToken } from '../constants';
import { User } from '@boilerplate/data';

describe('TestService', () => {
  let service: TestService;
  let usersRepo: typeof User;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        TestService,
        {
          provide: UsersRepositoryToken,
          useValue: User,
        },
      ],
    }).compile();

    service = module.get<TestService>(TestService);
    usersRepo = module.get(UsersRepositoryToken);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });

  describe('findAll', () => {
    it('should return an empty array', async () => {
      const result: User[] = [];
      jest.spyOn(usersRepo, 'findAll').mockImplementation(async () => result);

      expect(await service.findAll()).toBe(result);
    });
  });
});
