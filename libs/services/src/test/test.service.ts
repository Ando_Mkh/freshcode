import { Inject, Injectable } from '@nestjs/common';
import { UsersRepositoryToken } from '../constants';
import { User } from '@boilerplate/data';

@Injectable()
export class TestService {
  constructor(
    @Inject(UsersRepositoryToken) private readonly usersRepository: typeof User
  ) {}

  public async findAll(): Promise<User[]> {
    return this.usersRepository.findAll({ attributes: { exclude: ['updatedAt'] } });
  }
}
