import { Inject, Injectable } from '@nestjs/common';
import { UsersRepositoryToken } from './constants';
import {
  getLimitOptionFromFilter,
  getOffsetOptionFromFilter,
  getOrderOptionsFromFilter
} from './extensions/filters/base-filters-extensions';
import { UserCreationDto, UserDto, UserFilter } from '@boilerplate/shared';
import { User } from '@boilerplate/data';

interface All {
	filter: UserFilter;
}

@Injectable()
export class UsersService {
  constructor(
    @Inject(UsersRepositoryToken) private readonly usersRepository: typeof User,
  ) {
  }

  public async findAll({filter}: All): Promise<UserDto[]> {
    return this.usersRepository.findAll({
      order: getOrderOptionsFromFilter(filter),
      limit: getLimitOptionFromFilter(filter),
      offset: getOffsetOptionFromFilter(filter),
      raw: true,
    });
  }

  public async upsert(user: UserCreationDto): Promise<UserDto> {
    const test = await this.usersRepository.upsert(user, { returning: true });

    return test[0];
  }

  public async findByEmail(email: string): Promise<UserDto | null> {
  	return await this.usersRepository.findOne({ where: { email } });
	}

	public async findById(id: string): Promise<UserDto | null> {
  	return await this.usersRepository.findOne({where: {
  		id
			}});
	}

	public async updateUser(id: string, user: UserDto): Promise<void> {
  	await this.usersRepository.update(user, {
  		where: {
  			id
			}
		});
	}
}

