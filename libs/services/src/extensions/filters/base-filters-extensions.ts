import { Order } from 'sequelize/types/lib/model';
import { OrderFilter, PaginationFilter } from '@boilerplate/shared';

export function getOrderOptionsFromFilter<T>(filter: OrderFilter<T>): Order {
  return filter.orderedBy?.map(x => ([x.field, x.isReversed ? 'desc' : 'asc']));
}

export function getLimitOptionFromFilter(filter: PaginationFilter, defaultSize: number | null = 10): number | null {
  return filter.pageSize || defaultSize || null;
}

export function getOffsetOptionFromFilter(filter: PaginationFilter): number | null {
  if (filter.page)
    return getLimitOptionFromFilter(filter) * (filter.page - 1);
  else
    return null;
}
