import {
	AllowNull,
  Column,
  DataType,
  Model,
  PrimaryKey,
  Table,
} from 'sequelize-typescript';

@Table({
  tableName: 'users',
  timestamps: true
})
export class User extends Model {
  @PrimaryKey
  @Column({ type: DataType.UUID })
  public id: string;

  @Column
  public name: string;

	@AllowNull(false)
	@Column({ type: DataType.STRING })
	public email: string

	@AllowNull(false)
  @Column({ type: DataType.STRING })
	public password: string

  @Column({ type: DataType.DATE })
  public createdAt: Date;

  @Column({ type: DataType.DATE })
  public updatedAt: Date;
}
