import { BelongsTo, Column, DataType, Model, PrimaryKey, Table } from 'sequelize-typescript';
import { User } from './user.entity';

@Table({
  timestamps: false,
})
export class UserClientRole extends Model {
  @PrimaryKey
  @Column({ type: DataType.UUID })
  public id: string;

  @Column({ type: DataType.UUID })
  public userId: string;

  @Column({ type: DataType.UUID })
  public clientId: string;

  @Column({ type: DataType.UUID })
  public userRoleId: string;

  @BelongsTo(() => User, 'UserId')
  public user: User;
}

