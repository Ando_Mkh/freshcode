import { Sequelize } from 'sequelize-typescript';
import { Logger } from '@nestjs/common';
import * as Umzug from 'umzug';
import { ConfigService } from '@nestjs/config';
import {
	getSequelizeClient,
	getSequelizeClientMock,
} from './sequelize-client.factory';
import { Migration, MigrationContext } from './utility-types';
import { User, UserClientRole } from './models';
import initialMigration from './migrations/202101010000_initial';
import secondMigration from './migrations/202101010001_second';
import addColumnsToUsersMigration from './migrations/202110211501_add_columns_to_users';

const migrations: Migration[] = [
	initialMigration,
	secondMigration,
	addColumnsToUsersMigration,
];

const initializeSequelizeClient = async (
	sequelize: Sequelize,
	configService: ConfigService,
	logger: Logger
): Promise<Sequelize> => {
	sequelize.addModels([User, UserClientRole]);

	const transaction = await sequelize.transaction();
	const queryInterface = sequelize.getQueryInterface();
	let pendingMigrations: Umzug.Migration[];

	try {
		const umzug = new Umzug({
			migrations: Umzug.migrationsList(migrations, [
				{
					queryInterface,
					transaction,
					logger,
				} as MigrationContext,
			]),
			storage: 'sequelize',
			logging: (message) => {
				logger.log(message);
			},
			storageOptions: {
				sequelize,
			},
		});

		if (configService.get('DATABASE_ENABLE_MIGRATIONS') === 'true') {
			logger.log('Migrations are enabled. Running...');
			pendingMigrations = await umzug.pending();

			if (pendingMigrations.length) {
				await umzug.up();
				await transaction.commit();

				logger.log('Migrations complete');
			} else {
				logger.log('No pending migrations found');
			}
		}
	} catch (e) {
		logger.error('Exception occurred when migrating', e);
		await transaction.rollback();
		await queryInterface.bulkDelete('SequelizeMeta', {
			name: pendingMigrations.map((x) => x.file),
		});
	}

	return sequelize;
};

export const SequelizeFactory = async (
	configService: ConfigService
): Promise<Sequelize> => {
	const logger = new Logger('DB Init');

	logger.log('POSTGRES SEQUELIZE INSTANCE USED');

	return initializeSequelizeClient(
		getSequelizeClient(configService),
		configService,
		logger
	);
};

export const SequelizeMockFactory = async (
	configService: ConfigService
): Promise<Sequelize> => {
	const logger = new Logger('DB Init (MOCK)');

	logger.log('SQLITE IN-MEMORY SEQUELIZE INSTANCE USED');

	return initializeSequelizeClient(
		getSequelizeClientMock(),
		configService,
		logger
	);
};
