import { QueryInterface } from 'sequelize';
import { Transaction } from 'sequelize/types/lib/transaction';
import { Logger } from '@nestjs/common';

export interface Migration {
	name: string;
	up: MigrationFunc;
	down: MigrationFunc;
}

export interface MigrationContext {
    queryInterface: QueryInterface;
    transaction: Transaction;
    logger: Logger;
}

export type MigrationFunc = (context: MigrationContext) => Promise<void>;
