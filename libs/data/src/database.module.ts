import { Module } from '@nestjs/common';
import { ConfigModule, ConfigService } from '@nestjs/config';
import { SequelizeFactory } from './sequelize-provider.factory';
import { User } from './models/user.entity';
import { SequelizeToken, UsersRepositoryToken } from './constants';

export const databaseProviders = [
  {
    provide: SequelizeToken,
    useFactory: SequelizeFactory,
    inject: [ConfigService],
  },
  {
    provide: UsersRepositoryToken,
    useValue: User,
  }
];

@Module({
  imports: [ConfigModule],
  providers: [...databaseProviders],
  exports: [...databaseProviders],
})
export class DatabaseModule {}
