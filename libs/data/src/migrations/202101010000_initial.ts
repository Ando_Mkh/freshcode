import * as Sequelize from 'sequelize';
import { Migration } from '../utility-types';

export default {
	name: '202101010000_initial',

	up: async ({ queryInterface, transaction }) => {
		await queryInterface.createTable(
			'users',
			{
				id: {
					type: Sequelize.INTEGER,
					allowNull: false,
					primaryKey: true,
				},
				name: {
					type: Sequelize.STRING,
					allowNull: false,
				},
				createdAt: {
					type: Sequelize.DATE,
					allowNull: true,
				},
				updatedAt: {
					type: Sequelize.DATE,
					allowNull: true,
				},
			},
			{ transaction }
		);
	},

	down: async ({ queryInterface, transaction }) => {
		await queryInterface.dropTable('users', { transaction });
	},
} as Migration;
