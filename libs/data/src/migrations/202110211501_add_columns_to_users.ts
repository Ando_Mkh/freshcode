import * as Sequelize from 'sequelize';
import { Migration } from '../utility-types';

export default {
	name: '202110211501_add_columns_to_users',

	up: async ({ queryInterface, transaction }) => {
		await queryInterface.addColumn(
			'users',
			'email',
			{
				type: Sequelize.STRING,
				allowNull: false,
			},
			{ transaction }
		);
		await queryInterface.addColumn(
			'users',
			'password',
			{
				type: Sequelize.STRING,
				allowNull: false,
			},
			{ transaction }
		);
	},

	down: async ({ queryInterface, transaction }) => {
		await queryInterface.removeColumn('users', 'email', { transaction });
		await queryInterface.removeColumn('users', 'password', { transaction });
	},
} as Migration;
