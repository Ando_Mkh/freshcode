import { Migration } from '../utility-types';

export default {
	name: '202101010001_second',

	up: async ({ queryInterface, transaction }) => {
		await queryInterface.addIndex('users', {
			transaction,
			using: 'BTREE',
			fields: ['id', 'name'],
			name: 'IX_Users_Test_Index',
		});
	},

	down: async ({ queryInterface, transaction }) => {
		await queryInterface.removeIndex('users', 'IX_Users_Test_Index', {
			transaction,
		});
	},
} as Migration;
