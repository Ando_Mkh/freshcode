import { Sequelize } from 'sequelize-typescript';
import { ConfigService } from '@nestjs/config';

export const getSequelizeClient = (configService: ConfigService): Sequelize =>
    new Sequelize({
        dialect: 'postgres',
        host: configService.get<string>('DATABASE_HOST'),
        port: configService.get<number>('DATABASE_PORT'),
        username: configService.get<string>('DATABASE_USERNAME'),
        password: configService.get<string>('DATABASE_PASSWORD'),
        database: configService.get<string>('DATABASE_NAME'),
        logging: false
    });

export const getSequelizeClientMock = (): Sequelize => new Sequelize('sqlite::memory:');
