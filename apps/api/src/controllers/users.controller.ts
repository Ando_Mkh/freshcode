import { Body, Controller, Get, Put, Query, UseGuards, Res, Req, Post, Patch, Param } from '@nestjs/common';
import { Request, Response } from 'express';
import { AuthGuard } from '@nestjs/passport';
import { UsersService } from '@boilerplate/services';
import { TestDto, UserDto, UserFilter } from '@boilerplate/shared';
import { AuthService } from '@boilerplate/services';
import { sendEmail } from '../core/helpers/sendEmail';

@Controller('users')
export class UsersController {
  constructor(
    private readonly usersService: UsersService,
    private readonly authService: AuthService
  ) {}

  @Get()
  public async findAll(@Query() filter: UserFilter): Promise<UserDto[]> {
    try {
      return await this.usersService.findAll({ filter });
    } catch (e) {
      throw new Error('caught');
    }
  }

  @Put()
  public async create(@Body() entity: UserDto): Promise<UserDto> {
    return this.usersService.upsert(entity);
  }

	@UseGuards(AuthGuard('local'))
	@Get('/login')
	public async login(@Res({ passthrough: true }) res: Response, @Req() req: Request): Promise<string> {
		const user = req.user as UserDto;

		return this.authService.generateJWT(user);
	}

	@Post('/requestEmail/:email')
	public async requestEmail(@Param('email') email: string, @Res({ passthrough: true }) res: Response, @Req() req: Request): Promise<any> {
  	const user = await this.usersService.findByEmail(email);

		if (!user) {
			return res.status(404).send('The user with this email does not exist');
		}
		const token = this.authService.generateJWT(user);
  	sendEmail({
				to: [{email}],
			  subject: `Dear ${user.name},`,
				text: 'You recently requested to reset your password. To select a new password, click on the button below:',
				html: `<button>
						<a href="http://localhost:4200/forgot-password-confirm#token=${token}"></a>
					</button>`
		});

		return user;
	}

	@Post('/update-password')
	public async updatePassword(
		@Body() data: {password: string; confirm_password: string; token: string},
		@Res({ passthrough: true }) res: Response,
		@Req() req: Request) {
  	const userId = this.authService.parseJWT(data.token) as string;
		const user = await this.usersService.findById(userId);

		if (!user) {
			return res.status(401).send('The requested user does not exist');
		}

		if (data.password !== data.confirm_password) {
			return res.status(400).send('Passwords does not match');
		}
  	await this.usersService.updateUser(user.id, {
  		...user,
			password: data.password
		});

		return res.send('Updated');
	}

  @Get('test')
  public findAll1(): TestDto {
    return { id: 'kek', name: 'lol' } as TestDto;
  }
}
