import { Module } from '@nestjs/common';
import { ServicesModule } from '@boilerplate/services';
import { UsersController } from './users.controller';
import { HealthController } from './health.controller';
import { TerminusModule } from '@nestjs/terminus';
import { DatabaseModule } from '@boilerplate/data';

@Module({
  imports: [
    DatabaseModule,
    TerminusModule,
    ServicesModule,
  ],
  controllers: [
    UsersController,
    HealthController
  ],
})
export class ControllersModule {}
