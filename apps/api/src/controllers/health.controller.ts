import { Controller, Get, Inject } from '@nestjs/common';
import {
  HealthCheck,
  HealthCheckResult,
  HealthCheckService,
  SequelizeHealthIndicator
} from '@nestjs/terminus';
import { SequelizeToken } from '../constants';
import { Sequelize } from 'sequelize';

@Controller('health')
export class HealthController {
  constructor(
      @Inject(SequelizeToken) private readonly sequelize: Sequelize,
      private readonly healthCheckService: HealthCheckService,
      private readonly sequelizeHealthIndicator: SequelizeHealthIndicator
  ) {}

  @Get()
  @HealthCheck()
  public async check(): Promise<HealthCheckResult> {
    return await this.healthCheckService.check([
      async () => this.sequelizeHealthIndicator.pingCheck('database', { timeout: 1500, connection: this.sequelize })
    ]);
  }
}
