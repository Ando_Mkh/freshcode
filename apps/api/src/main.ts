import { NestFactory } from '@nestjs/core';
import {
  utilities as nestWinstonModuleUtilities,
  WinstonModule,
} from 'nest-winston';
import * as path from 'path';
import * as DailyRotateFile from 'winston-daily-rotate-file';
import * as swStats from 'swagger-stats';
import * as winston from 'winston';
import { ValidationPipe } from '@nestjs/common';
import { AppModule } from './app.module';
import { DocumentBuilder, SwaggerModule } from '@nestjs/swagger';
import AllExceptionsFilter from './exceptions-filters/all-exceptions.filter';
import BadRequestExceptionsFilter from './exceptions-filters/bad-request-exceptions.filter';

async function bootstrap(): Promise<void> {
  const app = await NestFactory.create(AppModule, {
    cors: true,
    logger: WinstonModule.createLogger({
      transports: [
        new winston.transports.Console({
          format: winston.format.combine(
              winston.format.timestamp(),
              winston.format.ms(),
              nestWinstonModuleUtilities.format.nestLike(),
          ),
          level: 'debug'
        }),
        new DailyRotateFile({
          format: winston.format.combine(
              winston.format.timestamp(),
              winston.format.ms(),
              winston.format.json(),
          ),
          filename: path.resolve('./logs', 'application-%DATE%.log'),
          datePattern: 'YYYY-MM-DD',
          zippedArchive: true,
          maxSize: '100m',
          maxFiles: '45d',
          level: 'debug'
        }),
      ],
    }),
  });

  const config = new DocumentBuilder()
    .setTitle('API')
    .setVersion('1.0')
    .build();
  const document = SwaggerModule.createDocument(app, config);
  SwaggerModule.setup('docs', app, document);

  app.useGlobalFilters(
    new AllExceptionsFilter(),
    new BadRequestExceptionsFilter()
  );

  app.useGlobalPipes(new ValidationPipe());

  app.use(swStats.getMiddleware({ swaggerSpec: document }));

  await app.listen(3000);
}

bootstrap();
