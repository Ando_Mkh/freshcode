export const SequelizeToken = 'SEQUELIZE';

export const config = {
	sendGrid: { apiKey: process.env.SENDGRID_API_KEY || '' },
	sendPulse: {
		userId: process.env.SENDPULSE_API_USER_ID || '',
		secret: process.env.SENDPULSE_API_SECRET || '',
		storage: process.env.SENDPULSE_TOKEN_STORAGE || '/tmp/'
	},
};
