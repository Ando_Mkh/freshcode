import { MiddlewareConsumer, Module } from '@nestjs/common';
import { ConfigModule } from '@nestjs/config';
import { ControllersModule } from './controllers/controllers.module';
import { ServicesModule } from '@boilerplate/services';
import { LoggerMiddleware } from './core/logging/logger.middleware';
import { DatabaseModule } from '@boilerplate/data';

const ENV = process.env.NODE_ENV;
const defaultEnvFilePath = '.env';
const defaultEnvLocalFilePath = `${defaultEnvFilePath}.local`;
const currentEnvFilePath = !ENV ? defaultEnvFilePath : `.env.${ENV}`;
const currentEnvLocalFilePath = `${currentEnvFilePath}.local`;

@Module({
  imports: [
    ConfigModule.forRoot({
      envFilePath: [currentEnvLocalFilePath, defaultEnvLocalFilePath, currentEnvFilePath, defaultEnvFilePath],
    }),
    DatabaseModule,
    ServicesModule,
    ControllersModule,
  ]
})
export class AppModule {
  public configure(consumer: MiddlewareConsumer): void {
    consumer.apply(LoggerMiddleware)
        .forRoutes('/');
  }
}
