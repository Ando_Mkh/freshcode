import {config} from '../../constants';
import {setApiKey} from '@sendgrid/mail';
import {init, smtpSendMail} from 'sendpulse-api';

setApiKey(config.sendGrid.apiKey);
init(config.sendPulse.userId, config.sendPulse.secret, config.sendPulse.storage, () => {});

type EmailConfig = {
	to: Array<{email: string}>;
	subject: string;
	text?: string;
	html: string;
};

export const sendEmail = (config: EmailConfig): void => {
	smtpSendMail(() => {}, {
		...config,
		from: {
			email: 'https://freshcodeit.com/',
			name: 'FreshCode'
		}
	});
};
