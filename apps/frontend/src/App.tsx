import React, { useEffect } from 'react';
import { RenderRouter } from './routes';
import { URLS } from './config/configParam.enum';
import {useHistory, useLocation} from 'react-router-dom';

export const App: React.FC = () => {
	const history = useHistory();
	const location = useLocation();
	useEffect(() => {
		if (!location.pathname.includes(URLS.FORGOT_PASS_CONFIRM) && !location.search.includes('token')) {
			history.push(URLS.LOGIN);
		}
	}, [history, location.pathname, location.search]);

	return (
		<RenderRouter />
	);
};
