import React from 'react';
import { rootStyles } from './styles';
import { Box, Button, Container, Input, InputLabel } from '@mui/material';
import { Link } from 'react-router-dom';
import { URLS } from '../../config/configParam.enum';
import ArrowBackIcon from '@mui/icons-material/ArrowBack';
import { useForgotPassword } from './useForgotPassword';
import { emailIsValid } from '../../config/helpers';

export const ForgotPassword: React.FC = () => {
	const { email, handleChangeEmail, onConfirm, emailSends } = useForgotPassword();

	return (
		<Container sx={rootStyles.body}>
			<Box sx={rootStyles.form}>
				<Box>
					<Link to={URLS.LOGIN} style={rootStyles.backLink} className='mr-1'>
						<ArrowBackIcon className='mr-1' />
						Back
					</Link>
					<Box sx={rootStyles.title}>
						Forgot your password ?
					</Box>
				</Box>
				{emailSends
					? <p>Thanks you will recive an email to verify </p>
					: (
						<>
							<Box sx={rootStyles.subtitle}>
								Enter your email address and we'll send you a ink to reset your password.
							</Box>
							<InputLabel htmlFor='email-input'>Email</InputLabel>
							<Input
								value={email}
								onChange={handleChangeEmail}
								id='email-input'
								sx={rootStyles.input}
							/>

							<Button
								sx={emailIsValid(email) ? rootStyles.button : rootStyles.buttonDisabled}
								onClick={onConfirm}
							>
								Send a recovery link
							</Button>
						</>)}
			</Box>
		</Container>
	);
};
