import { ChangeEvent, useState } from 'react';
import { usersService } from '../../data-services';

type UseForgotPasswordOutput = {
  email: string;
	handleChangeEmail(e: ChangeEvent<HTMLInputElement>): void;
	onConfirm(): void;
	emailSends: boolean;
};

export const useForgotPassword = () => {
	const [email, setEmail] = useState<string>('');
	const [emailSends, setEmailSends] = useState<boolean>(false);
	const handleChangeEmail = (e: ChangeEvent<HTMLInputElement>): void => {
		setEmail(e.target.value);
	};
	const onConfirm = (): void => {
		usersService
			.requestSendEmail(email)
			.then(res => { setEmailSends(res); })
		;
	};

	return {
		email,
		emailSends,
		handleChangeEmail,
		onConfirm
	};
};
