export const rootStyles = {
	body: {
		width: '100vw',
		height: '100vh',
		display: 'flex',
		justifyContent: 'center',
		alignItems: 'center'
	},
	form: {
		width: '100%',
		maxWidth: '700px',
		backgroundColor: 'rgba(0, 0, 0, .07)',
		borderRadius: '20px',
		p: '30px'
	},
	title: {
		display: 'flex',
		justifyContent: 'center',
		mb: '30px',
		fontSize: '26px'
	},
	subtitle: {
		display: 'flex',
		justifyContent: 'center',
		mb: '30px',
		fontSize: '20px'
	},
	input: {
		width: '100%'
	},
	link: {
		color: 'blue',
		textDecoration: 'underline'
	},
	buttonDisabled: {
		backgroundColor: 'silver',
		'&:hover': {
			backgroundColor: 'silver',
			color: 'silver',
		},
		ml: 'calc(50% - 75px)',
		mt: '10px',
	},
	button: {
		ml: 'calc(50% - 75px)',
		mt: '10px',
		color: 'white',
		backgroundColor: 'blue',
	},
	error: {
		color: 'red',
		mb: '20px',
		fontSize: '12px',
	},
	backLink: {
		fontSize: '16px',
		alignItems: 'center',
		display: 'flex',
		marginRight: '10px'
	}
};
