export const rootStyles = {
	body: {
		width: '100vw',
		height: '100vh',
		display: 'flex',
		justifyContent: 'center',
		alignItems: 'center'
	},
	form: {
		width: '400px',
		backgroundColor: 'rgba(0, 0, 0, .07)',
		borderRadius: '20px',
		p: '30px'
	},
	title: {
		display: 'flex',
		justifyContent: 'center',
		mb: '30px',
		fontSize: '26px'
	},
	subtitle: {
		display: 'flex',
		justifyContent: 'center',
		mb: '30px',
		fontSize: '20px'
	},
	input: {
		width: '100%'
	},
	link: {
		color: 'blue',
		textDecoration: 'underline',
		cursor: 'pointer',
	},
	button: {
		width: '150px',
		ml: 'calc(50% - 75px)',
		mt: '10px',
		color: 'white',
		backgroundColor: 'blue',
		'&:hover': {
			backgroundColor: 'silver',
		},
	},
	error: {
		color: 'red',
		mb: '20px',
		fontSize: '12px',
	},
	backLink: {
		fontSize: '16px',
		alignItems: 'center',
		display: 'flex',
		marginRight: '10px'
	}
};
