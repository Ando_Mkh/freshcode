import { useState } from 'react';
import { useForm } from 'react-hook-form';
import { LoginDto } from '@boilerplate/shared';
import { usersService } from '../../data-services';
import { classValidatorResolver } from '@hookform/resolvers/class-validator';

interface UseLogin {
	errors: any;
	register: any;
	showPassword: boolean;
	onConfirm(): Promise<void>;
	handleClickShowPassword(): void;
}

export const useLogin = (): UseLogin => {
	const resolver = classValidatorResolver(LoginDto);
	const [showPassword, setShowPassword] = useState(false);
	const { handleSubmit, register, formState: { errors } } = useForm<LoginDto>({
		defaultValues: { email: '', password: '' },
		resolver
	});
	const handleClickShowPassword = (): void => {
		setShowPassword(!showPassword);
	};
	const onConfirm = async (): Promise<void> => {
		await handleSubmit(
			async value => {
				try {
					const { data: token } = await usersService.login(value);
					console.info(token);
				} catch (e) {
					console.info(e.message);
				}
			}
		)();
	};

	return {
		errors,
		register,
		showPassword,
		handleClickShowPassword,
		onConfirm,
	};
};
