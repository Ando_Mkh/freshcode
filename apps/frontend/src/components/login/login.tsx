import React, { FC } from 'react';
import { Box, Button, Container, Input, InputLabel } from '@mui/material';
import { rootStyles } from './styles/root.styles';
import Visibility from '@mui/icons-material/Visibility';
import VisibilityOff from '@mui/icons-material/VisibilityOff';
import InputAdornment from '@mui/material/InputAdornment';
import IconButton from '@mui/material/IconButton';
import { useLogin } from './useLogin';
import { Link } from 'react-router-dom';
import { URLS } from '../../config/configParam.enum';


export const Login: FC = () => {
	const { onConfirm, register, errors, showPassword, handleClickShowPassword } = useLogin();

	return (
		<Container sx={rootStyles.body}>
			<Box sx={rootStyles.form}>
				<Box sx={rootStyles.title}>Login</Box>
				<InputLabel htmlFor='email-input'>Email</InputLabel>
				<Input
					{...register('email')}
					id='email-input'
					sx={rootStyles.input}
				/>
				<Box sx={rootStyles.error}>{errors.email?.message}</Box>
				<InputLabel htmlFor='password-input'>Password</InputLabel>
				<Input
					{...register('password')}
					id='password-input'
					type={showPassword ? 'text' : 'password'}
					sx={rootStyles.input}
					endAdornment={
						<InputAdornment position='end'>
							<IconButton
								aria-label='toggle password visibility'
								onClick={handleClickShowPassword}
								edge='end'
							>
								{showPassword ? <VisibilityOff /> : <Visibility />}
							</IconButton>
						</InputAdornment>
					}
				/>
				<Link to={URLS.FORGOT_PASS} style={rootStyles.link}>Forgot password ?</Link>
				<Box sx={rootStyles.error}>{errors.password?.message}</Box>
				<Button
					sx={rootStyles.button}
					onClick={onConfirm}
				>
					Confirm
				</Button>
			</Box>
		</Container>
	);
};

export default Login;
