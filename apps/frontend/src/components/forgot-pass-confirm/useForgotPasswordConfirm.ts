import { ChangeEvent, useEffect, useState } from 'react';
import { useHistory, useLocation } from 'react-router-dom';
import querystring from 'query-string';
import { usersService } from '../../data-services';
import { URLS } from '../../config/configParam.enum';

type Creds = {
	password: string;
	confirm_password: string;
};

type UseForgotPasswordConfirmOutput = {
	showPassword: boolean;
	showConfirmPassword: boolean;
	credentials: Creds;
	handleChange(e: ChangeEvent<HTMLInputElement>): void;
	togglePasswordShow(): void;
	togglePasswordConfirmShow(): void;
	onConfirm(): void;
};

export const useForgotPasswordConfirm = (): UseForgotPasswordConfirmOutput => {
	const history = useHistory();
	const [showPassword, setShowPassword] = useState(false);
	const [showConfirmPassword, setShowConfirmPassword] = useState(false);
	const [credentials, setCredentials] = useState<Creds>({
		confirm_password: '',
		password: ''
	});

	const [token, setToken] = useState('');

	const handleChange = (e: ChangeEvent<HTMLInputElement>) => {
		setCredentials(prev => ({
			...prev,
			[e.target.name]: e.target.value
		}));
	};

	const onConfirm = (): void => {
		usersService.updatePassword({
			...credentials,
			token
		}).then(() => {
			history.push(URLS.LOGIN);
		});
	};
	const togglePasswordShow = (): void => {
		setShowPassword(!showPassword);
	};
	const togglePasswordConfirmShow = (): void => {
		setShowConfirmPassword(!showConfirmPassword);
	};
	const location = useLocation();

	useEffect(() => {
		if (location.hash.includes('token')) {
			const parsed = querystring.parse(location.hash);
			const token = 'token' in querystring.parse(location.hash)
			? parsed.token as string
				: '';

			setToken(token);
		}
	}, [location.hash]);

	return {
		showPassword,
		showConfirmPassword,
		credentials,
		handleChange,
		onConfirm,
		togglePasswordShow,
		togglePasswordConfirmShow
	};
};
