import React from 'react';
import { rootStyles } from '../login/styles/root.styles';
import { Box, Button, Container, Input, InputLabel } from '@mui/material';
import InputAdornment from '@mui/material/InputAdornment';
import IconButton from '@mui/material/IconButton';
import VisibilityOff from '@mui/icons-material/VisibilityOff';
import Visibility from '@mui/icons-material/Visibility';
import { useForgotPasswordConfirm } from './useForgotPasswordConfirm';


export const ForgotPassConfirm: React.FC = () => {
	const {
		showPassword,
		showConfirmPassword,
		credentials,
		handleChange,
		onConfirm,
		togglePasswordShow,
		togglePasswordConfirmShow
	} = useForgotPasswordConfirm();

	return (
		<Container sx={rootStyles.body}>
			<Box sx={rootStyles.form}>
				<Box sx={rootStyles.title}>
					Please Confirm your new password
				</Box>
				<InputLabel htmlFor='password-input'>Password</InputLabel>
				<Input
					value={credentials.password}
					id='password-input'
					type={showPassword ? 'text' : 'password'}
					sx={rootStyles.input}
					name={'password'}
					onChange={handleChange}
					endAdornment={
						<InputAdornment position='end'>
							<IconButton
								aria-label='toggle password visibility'
								onClick={togglePasswordShow}
								edge='end'
							>
								{showPassword ? <VisibilityOff /> : <Visibility />}
							</IconButton>
						</InputAdornment>
					}
				/>
				<InputLabel htmlFor='confirm-password-input'>Confirm Password</InputLabel>
				<Input
					onChange={handleChange}
					value={credentials.confirm_password}
					id='confirm-password-input'
					type={showConfirmPassword ? 'text' : 'password'}
					sx={rootStyles.input}
					name={'confirm_password'}
					endAdornment={
						<InputAdornment position='end'>
							<IconButton
								aria-label='toggle password visibility'
								onClick={togglePasswordConfirmShow}
								edge='end'
							>
								{showConfirmPassword ? <VisibilityOff /> : <Visibility />}
							</IconButton>
						</InputAdornment>
					}
				/>
				<Button
					sx={rootStyles.button}
					onClick={onConfirm}
				>
					Confirm
				</Button>
			</Box>
		</Container>
	);
};
