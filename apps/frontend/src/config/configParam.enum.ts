export enum ConfigParam {
	API_URL = 'NX_APP_API_URL'
}

interface IURLS {
	LOGIN: '/login';
	FORGOT_PASS: '/forgot-password';
	FORGOT_PASS_CONFIRM: '/forgot-password-confirm';
}

export const URLS: IURLS = {
	LOGIN: '/login',
	FORGOT_PASS: '/forgot-password',
	FORGOT_PASS_CONFIRM: '/forgot-password-confirm'
};
