import { AxiosInstance, AxiosResponse } from 'axios';
import { LoginDto, UserDto, UserFilter } from '@boilerplate/shared';

class UsersService {
	private readonly forgotPassToken: string;

  constructor(private readonly httpClient: AxiosInstance) {
		this.forgotPassToken = '';
	}

  public async getTestData(filters?: UserFilter): Promise<AxiosResponse<UserDto[]>> {
    return this.httpClient.get<UserDto[]>('users', { params: filters });
  }

	public async login(body: LoginDto): Promise<AxiosResponse<string | undefined>> {
		return this.httpClient.put<string | undefined>('users/login', body);
	}

	public async requestSendEmail(email: string): Promise<any> {
  	try {
			const response =  await this.httpClient.post(`users/requestEmail/${email}`);

			return Boolean(response.data);
		} catch (err) {
  		console.log(err);
		}
	}

	public async updatePassword(form: {password: string; confirm_password: string; token: string}): Promise<void> {
  	try {
  		debugger;
  		await this.httpClient.post(`users/update-password/`, form);
		} catch (err) {
  		console.log(err);
		}
	}
}

export default UsersService;
