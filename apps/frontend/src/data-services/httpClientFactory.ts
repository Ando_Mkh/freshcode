import axios, { AxiosInstance, AxiosRequestConfig } from 'axios';
import { ConfigParam } from '../config/configParam.enum';
import { ConfigService } from '../config/configService';

const httpClientFactory = (configService: ConfigService): AxiosInstance => {
  const httpClient = axios.create({
    baseURL: configService.get<string>(ConfigParam.API_URL),
    timeout: 30000,
    headers: { 'X-Text-Header': 'test' },
  });

  const authHeaderInterceptor = (config: AxiosRequestConfig): AxiosRequestConfig => {
    const tokenFromLocalStorage = localStorage.getItem('token');

    if (tokenFromLocalStorage) {
      config.headers.Authorization = `Bearer ${tokenFromLocalStorage}`;
    }

    return config;
  };

  httpClient.interceptors.request.use(authHeaderInterceptor);

  return httpClient;
};

export default httpClientFactory;
