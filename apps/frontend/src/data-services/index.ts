import UsersService from './UsersService';
import httpClientFactory from './httpClientFactory';
import configService from '../config/configService';
import DependentService from './DependentService';

export const httpClient = httpClientFactory(configService);

export const usersService = new UsersService(httpClient);
export const dependentService = new DependentService(usersService);
