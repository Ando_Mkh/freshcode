import UsersService from './UsersService';
import { UserDto } from '@boilerplate/shared';

class DependentService {
  constructor(private readonly usersService: UsersService) {
  }

  public async getTestData(): Promise<UserDto[]> {
    const { data: response } = await this.usersService.getTestData();

    return response;
  }
}

export default DependentService;
