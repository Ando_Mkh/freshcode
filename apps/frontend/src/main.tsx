import * as ReactDOM from 'react-dom';
import { RootThemeProvider } from './RootThemeProvider';
import { BrowserRouter } from 'react-router-dom';
import { CssBaseline } from '@mui/material';
import { App } from './App';

ReactDOM.render(
		<RootThemeProvider>
			<BrowserRouter>
				<CssBaseline />
				<App />
			</BrowserRouter>
		</RootThemeProvider>,
	document.getElementById('root')
);
