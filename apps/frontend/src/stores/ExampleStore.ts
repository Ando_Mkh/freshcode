import { action, makeObservable, observable } from 'mobx';
import { MouseEvent } from 'react';


export class ExampleStore {
  @observable public textProp = {
    text: '24323'
  };

  @observable public booleanProp = false;

  constructor() {
    makeObservable(this);
  }

  @action('test function')
  public setTextProp = (e: MouseEvent): void => {
    console.info('e', e);
    this.textProp.text += 'test2';
    this.booleanProp = true;
  };
}
