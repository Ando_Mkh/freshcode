import React from 'react';
import { Route, Switch } from 'react-router-dom';
import Login from './components/login/login';
import { URLS } from './config/configParam.enum';
import { ForgotPassword } from './components/forgot-pass';
import { ForgotPassConfirm } from './components/forgot-pass-confirm';


export const RenderRouter: React.FC = () => (
		<Switch>
			<Route component={Login} exact path={URLS.LOGIN}/>
			<Route component={ForgotPassword} exact path={URLS.FORGOT_PASS}/>
			<Route component={ForgotPassConfirm} path={URLS.FORGOT_PASS_CONFIRM}/>
		</Switch>
	);
