FROM node:16-alpine as builder

COPY . .

RUN yarn install && npx nx build frontend

FROM nginx:alpine as runner
COPY --from=builder /dist/apps/frontend /usr/share/nginx/html

EXPOSE 4200

CMD ["/bin/bash", "-c", "cd /usr/share/nginx/html/ && nginx -g \"daemon off;\""]
